package edu.sdust.sheny.service;

import com.github.pagehelper.PageInfo;
import edu.sdust.sheny.goods.pojo.Brand;

import java.util.List;

/**
 * @author sheny
 */
public interface BrandService {

    /**
     * select brands by page and information.
     * @param brand The rule.
     * @param page The number of pages.
     * @param size The number of each page.
     * @return
     */
    PageInfo<Brand> findPage(Brand brand, Integer page, Integer size);

    /**
     * select brands by page.
     * @param page The number of pages.
     * @param size The number of each page.
     * @return
     */
    PageInfo<Brand> findPage(Integer page, Integer size);

    /**
     * select brands by information.
     *
     * @param brand
     * @return
     */
    List<Brand> findList(Brand brand);

    /**
     * delete a brand by id.
     *
     * @param id
     */
    void delete(Integer id);

    /**
     * Update a brand by id.
     *
     * @param brand
     */
    void update(Brand brand);

    /**
     * Add a brad.
     *
     * @param brand Brand.
     */
    void add(Brand brand);

    /**
     * Select all brand.
     *
     * @return List of brands.
     */
    List<Brand> findAll();

    /**
     * Select brand where id = id.
     *
     * @param id
     * @return Be selected brand.
     */
    Brand findById(Integer id);
}
