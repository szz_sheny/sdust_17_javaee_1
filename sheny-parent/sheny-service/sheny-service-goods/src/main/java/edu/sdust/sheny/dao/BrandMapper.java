package edu.sdust.sheny.dao;
import edu.sdust.sheny.goods.pojo.Brand;
import tk.mybatis.mapper.common.Mapper;

/**
 * Using normally Mapper.
 *
 * Add data -> using {@code Mapper.insert()}
 * Add data -> using {@code Mapper.insertSelective()}
 *
 * Update data -> using {@code Mapper.update(T)}
 * Update data -> using {@code Mapper.updateByPrimary(T)}
 *
 * Select data -> using {@code Mapper.selectByPrimary(id)}
 * select data -> using {@code Mapper.select(T)}
 *
 * @author sheny
 */
public interface BrandMapper extends Mapper<Brand> {
}
