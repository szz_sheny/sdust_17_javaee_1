package edu.sdust.sheny.controller;

import com.github.pagehelper.PageInfo;
import edu.sdust.sheny.goods.pojo.Brand;
import edu.sdust.sheny.service.BrandService;
import edu.sdust.sheny.entity.Result;
import edu.sdust.sheny.entity.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author sheny
 */
@RestController
@RequestMapping(value = "/brand")
@CrossOrigin
public class BrandController {

    private final BrandService brandService;

    @Autowired
    public BrandController(BrandService brandService) {
        this.brandService = brandService;
    }

    @PostMapping(value = "/search/{page}/{size}")
    public Result<PageInfo<Brand>> findPage(@RequestBody Brand brand,
                                             @PathVariable Integer page, @PathVariable Integer size) {
        PageInfo<Brand> pageInfo = brandService.findPage(brand, page, size);
        return new Result<PageInfo<Brand>>(true, StatusCode.OK,
                "You have selected a page info list of brands and information successfully!", pageInfo);
    }

    @GetMapping(value = "/search/{page}/{size}")
    public Result<PageInfo<Brand>> findPage(@PathVariable Integer page, @PathVariable Integer size) {
        PageInfo<Brand> pageInfo = brandService.findPage(page, size);
        return new Result<PageInfo<Brand>>(true, StatusCode.OK,
                "You have selected a page info list of brands successfully!", pageInfo);
    }


    @PostMapping(value = "/search")
    public Result<List<Brand>> findList(@RequestBody Brand brand) {
        List<Brand> brands = brandService.findList(brand);
        return new Result<List<Brand>>(true, StatusCode.OK,
                "You have searched a list of brand successfully!", brands);
    }

    @DeleteMapping(value = "/{id}")
    public Result delete(@PathVariable("id") Integer id) {
        brandService.delete(id);
        return new Result(true, StatusCode.OK,
                "You have deleted a brand successfully!");
    }

    @PutMapping(value = "/{id}")
    public Result update(@PathVariable(value = "id") Integer id, @RequestBody Brand brand) {
        brand.setId(id);
        brandService.update(brand);
        return new Result(true, StatusCode.OK,
                "You have updated a brand successfully!");
    }

    @PostMapping
    public Result add(@RequestBody Brand brand) {
        brandService.add(brand);
        return new Result(true, StatusCode.OK,
                "You have added a brand successfully!");
    }

    @GetMapping(value = "/{id}")
    public Result<Brand> findById(@PathVariable(value = "id") Integer id) {
        Brand brand = brandService.findById(id);
        return new Result<Brand>(true, StatusCode.OK,
                "You have selected the brand successfully!", brand);
    }

    @GetMapping
    public Result<List<Brand>> findAll() {
        List<Brand> brands = brandService.findAll();
        return new Result<List<Brand>>(true, StatusCode.OK,
                "You have selected brands successfully!", brands);
    }
}
