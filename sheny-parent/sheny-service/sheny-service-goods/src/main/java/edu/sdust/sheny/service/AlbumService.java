package edu.sdust.sheny.service;

import com.github.pagehelper.PageInfo;
import edu.sdust.sheny.goods.pojo.Album;

import java.util.List;

/**
 * @author sheny
 */
public interface AlbumService {

    /**
     * select albums by page.
     *
     * @param page The number of pages.
     * @param size The number of each page.
     * @return
     */
    PageInfo<Album> findPage(Integer page, Integer size);

    /**
     * delete a album by id.
     *
     * @param id
     */
    void delete(Integer id);

    /**
     * Update a album by id.
     *
     * @param album
     */
    void update(Album album);

    /**
     * Add a brad.
     *
     * @param album album.
     * @return
     */
    void add(Album album);

    /**
     * Select all album.
     *
     * @return List of albums.
     */
    List<Album> findAll();

    /**
     * Select album where id = id.
     *
     * @param id
     * @return Be selected album.
     */
    Album findById(Integer id);
}
