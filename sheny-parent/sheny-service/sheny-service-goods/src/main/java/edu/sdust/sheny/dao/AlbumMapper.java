package edu.sdust.sheny.dao;

import edu.sdust.sheny.goods.pojo.Album;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author sheny
 */
public interface AlbumMapper extends Mapper<Album> {
}
