package edu.sdust.sheny.controller;

import edu.sdust.sheny.entity.Result;
import edu.sdust.sheny.entity.StatusCode;
import edu.sdust.sheny.file.FastDFSFile;
import edu.sdust.sheny.util.FastDFSUtil;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author sheny
 */
@RestController
@RequestMapping(value = "/upload")
@CrossOrigin
public class FileUpLoadController {

    @PostMapping
    public Result upload(@RequestParam(value = "file")MultipartFile file) throws Exception {
        FastDFSFile fastDFSFile = new FastDFSFile(
                file.getOriginalFilename(),
                file.getBytes(),
                StringUtils.getFilenameExtension(file.getOriginalFilename())
        );
        FastDFSUtil.upload(fastDFSFile);
        return new Result(true, StatusCode.OK, "You have updated file successfully!", null);
    }

}
