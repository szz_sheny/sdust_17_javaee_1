package edu.sdust.sheny;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * Open eureka's service.
 * @author sheny
 */
@SpringBootApplication
@EnableEurekaServer
public class EurekaApplication {

    /**
     * Loading starter class.
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(EurekaApplication.class, args);

    }
}
